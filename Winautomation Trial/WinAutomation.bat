@echo off
setLocal EnableDelayedExpansion

SET TODAY=%date:~4,2%-%date:~7,2%-%date:~12,2%

REM setting date back to an older date
DATE 12-1-14
start "" "C:\Program Files\WinAutomation\WinAutomation.Console.exe"
REM Changing date back to todays date
rem echo "Press Any Key To change system date to today"
rem PAUSE > nul
rem SLEEP 10
rem EXIT
TIMEOUT 5
DATE %TODAY%
rem echo %date%
rem PAUSE